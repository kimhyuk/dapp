// Allows us to use ES6 in our migrations and tests.
require('babel-register')
var HDWalletProvider = require("truffle-hdwallet-provider");
//load single private key as string
//하드웨어지갑 프로바이더
var provider = new HDWalletProvider("6da79cd690d025bc43b36660241f363046cb9013549112e4db2f12d5955e05ba", "http://localhost:8545");
module.exports = {
  contracts_build_directory:"../server/uploads",
  networks: {
    development: {
      host: '127.0.0.1',
      port: 8545,
      // provider: () => provider,
      // from: "0xCc6DA469c01Ac0884B1740fc74B89B5c68C6E45c",
      network_id: '*' // Match any network id
    }
  }
}